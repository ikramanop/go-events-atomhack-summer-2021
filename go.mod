module events-backend

go 1.15

require (
	github.com/go-openapi/errors v0.20.1
	github.com/go-openapi/loads v0.20.2
	github.com/go-openapi/runtime v0.19.31
	github.com/go-openapi/spec v0.20.3
	github.com/go-openapi/strfmt v0.20.2
	github.com/go-openapi/swag v0.19.15
	github.com/go-openapi/validate v0.20.1
	github.com/go-pg/pg/v10 v10.10.4
	github.com/jessevdk/go-flags v1.5.0
	github.com/satori/go.uuid v1.2.0
	golang.org/x/net v0.0.0-20210226172049-e18ecbb05110
)
