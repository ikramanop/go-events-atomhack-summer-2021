package handlers

import (
	"log"

	"events-backend/internal/database"
	"events-backend/internal/database/methods"
	"events-backend/internal/generated/restapi/operations"

	"github.com/go-openapi/runtime/middleware"
)

func GetEventsHandler(params operations.GetEventsParams) middleware.Responder {
	log.Printf("Hit GET /events from %s\n", params.HTTPRequest.UserAgent())

	db := database.NewDBConnector(true)
	defer db.Close()

	events, err := methods.GetEvents(db, params)
	if err != nil {
		log.Println(err)

		log.Printf("Error returning events")

		return operations.NewGetEventsBadRequest().WithPayload(&operations.GetEventsBadRequestBody{
			ErrorCode: 12,
			Status:    false,
		})
	}

	return operations.NewGetEventsOK().WithPayload(
		events,
	)
}
