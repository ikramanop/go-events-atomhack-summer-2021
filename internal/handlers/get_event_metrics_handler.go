package handlers

import (
	"log"

	"events-backend/internal/database"
	"events-backend/internal/database/methods"
	"events-backend/internal/generated/restapi/operations"

	"github.com/go-openapi/runtime/middleware"
)

func GetEventMetricsHandler(params operations.GetEventMetricsParams) middleware.Responder {
	log.Printf("Hit GET /events from %s\n", params.HTTPRequest.UserAgent())

	db := database.NewDBConnector(true)
	defer db.Close()

	metrics, err := methods.GetEventMetrics(db, params)
	if err != nil {
		log.Println(err)

		log.Printf("Error returning events")

		return operations.NewGetEventMetricsBadRequest().WithPayload(&operations.GetEventMetricsBadRequestBody{
			ErrorCode: 12,
			Status:    false,
		})
	}

	return operations.NewGetEventMetricsOK().WithPayload(
		metrics,
	)
}
