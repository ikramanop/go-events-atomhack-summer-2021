package handlers

import (
	"log"

	"events-backend/internal/database"
	"events-backend/internal/database/methods"
	"events-backend/internal/generated/restapi/operations"

	"github.com/go-openapi/runtime/middleware"
)

func GetEventsMetricsHandler(params operations.GetEventsMetricsParams) middleware.Responder {
	log.Printf("Hit GET /events from %s\n", params.HTTPRequest.UserAgent())

	db := database.NewDBConnector(true)
	defer db.Close()

	metrics, err := methods.GetEventsMetrics(db)
	if err != nil {
		log.Println(err)

		log.Printf("Error returning events")

		return operations.NewGetEventsMetricsBadRequest().WithPayload(&operations.GetEventsMetricsBadRequestBody{
			ErrorCode: 12,
			Status:    false,
		})
	}

	return operations.NewGetEventsMetricsOK().WithPayload(
		metrics,
	)
}
