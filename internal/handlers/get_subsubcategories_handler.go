package handlers

import (
	"log"

	"events-backend/internal/database"
	"events-backend/internal/database/methods"
	"events-backend/internal/generated/restapi/operations"

	"github.com/go-openapi/runtime/middleware"
)

func GetSubSubCategoriesHandler(params operations.GetSubSubCategoriesParams) middleware.Responder {
	log.Printf("Hit GET /events from %s\n", params.HTTPRequest.UserAgent())

	db := database.NewDBConnector(true)
	defer db.Close()

	categories, err := methods.GetSubSubCategories(db, params)
	if err != nil {
		log.Println(err)

		log.Printf("Error returning sub sub categories")

		return operations.NewGetSubSubCategoriesBadRequest().WithPayload(&operations.GetSubSubCategoriesBadRequestBody{
			ErrorCode: 12,
			Status:    false,
		})
	}

	return operations.NewGetSubSubCategoriesOK().WithPayload(
		categories,
	)
}
