package handlers

import (
	"log"

	"events-backend/internal/database"
	"events-backend/internal/database/methods"
	"events-backend/internal/generated/restapi/operations"

	"github.com/go-openapi/runtime/middleware"
)

func BookEventHandler(params operations.BookEventParams) middleware.Responder {
	log.Printf("Hit GET /events from %s\n", params.HTTPRequest.UserAgent())

	db := database.NewDBConnector(true)
	defer db.Close()

	err := methods.BookEvent(db, params)
	if err != nil {
		log.Println(err)

		log.Printf("Error liking event")

		return operations.NewBookEventBadRequest().WithPayload(&operations.BookEventBadRequestBody{
			ErrorCode: 12,
			Status:    false,
		})
	}

	return operations.NewBookEventOK().WithPayload(
		&operations.BookEventOKBody{Status: true},
	)
}
