package handlers

import (
	"log"

	"events-backend/internal/database"
	"events-backend/internal/database/methods"
	"events-backend/internal/generated/restapi/operations"

	"github.com/go-openapi/runtime/middleware"
)

func DislikeEventHandler(params operations.DislikeEventParams) middleware.Responder {
	log.Printf("Hit GET /events from %s\n", params.HTTPRequest.UserAgent())

	db := database.NewDBConnector(true)
	defer db.Close()

	status, err := methods.DislikeEvent(db, params)
	if err != nil {
		log.Println(err)

		log.Printf("Error liking event")

		return operations.NewDislikeEventBadRequest().WithPayload(&operations.DislikeEventBadRequestBody{
			ErrorCode: 12,
			Status:    false,
		})
	}

	if !status {
		return operations.NewDislikeEventBadRequest().WithPayload(&operations.DislikeEventBadRequestBody{
			ErrorCode: 12,
			Status:    false,
		})
	}

	return operations.NewDislikeEventOK().WithPayload(
		&operations.DislikeEventOKBody{Status: status},
	)
}
