package handlers

import (
	"log"

	"events-backend/internal/database"
	"events-backend/internal/database/methods"
	"events-backend/internal/generated/restapi/operations"

	"github.com/go-openapi/runtime/middleware"
)

func GetSubCategoriesHandler(params operations.GetSubCategoriesParams) middleware.Responder {
	log.Printf("Hit GET /events from %s\n", params.HTTPRequest.UserAgent())

	db := database.NewDBConnector(true)
	defer db.Close()

	categories, err := methods.GetSubCategories(db, params)
	if err != nil {
		log.Println(err)

		log.Printf("Error returning subcategories")

		return operations.NewGetSubCategoriesBadRequest().WithPayload(&operations.GetSubCategoriesBadRequestBody{
			ErrorCode: 12,
			Status:    false,
		})
	}

	return operations.NewGetSubCategoriesOK().WithPayload(
		categories,
	)
}
