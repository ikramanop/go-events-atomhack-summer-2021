package handlers

import (
	"log"

	"events-backend/internal/database"
	"events-backend/internal/database/methods"
	"events-backend/internal/generated/restapi/operations"

	"github.com/go-openapi/runtime/middleware"
)

func GetCategoriesHandler(params operations.GetCategoriesParams) middleware.Responder {
	log.Printf("Hit GET /events from %s\n", params.HTTPRequest.UserAgent())

	db := database.NewDBConnector(true)
	defer db.Close()

	categories, err := methods.GetCategories(db)
	if err != nil {
		log.Println(err)

		log.Printf("Error returning categories")

		return operations.NewGetCategoriesBadRequest().WithPayload(&operations.GetCategoriesBadRequestBody{
			ErrorCode: 12,
			Status:    false,
		})
	}

	return operations.NewGetCategoriesOK().WithPayload(
		categories,
	)
}
