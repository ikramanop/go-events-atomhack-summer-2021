package handlers

import (
	"log"

	"events-backend/internal/database"
	"events-backend/internal/database/methods"
	"events-backend/internal/generated/restapi/operations"

	"github.com/go-openapi/runtime/middleware"
)

func LikeEventHandler(params operations.LikeEventParams) middleware.Responder {
	log.Printf("Hit GET /events from %s\n", params.HTTPRequest.UserAgent())

	db := database.NewDBConnector(true)
	defer db.Close()

	status, err := methods.LikeEvent(db, params)
	if err != nil {
		log.Println(err)

		log.Printf("Error liking event")

		return operations.NewLikeEventBadRequest().WithPayload(&operations.LikeEventBadRequestBody{
			ErrorCode: 12,
			Status:    false,
		})
	}

	if !status {
		return operations.NewLikeEventBadRequest().WithPayload(&operations.LikeEventBadRequestBody{
			ErrorCode: 12,
			Status:    false,
		})
	}

	return operations.NewLikeEventOK().WithPayload(
		&operations.LikeEventOKBody{Status: status},
	)
}
