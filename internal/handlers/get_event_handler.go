package handlers

import (
	"log"

	"events-backend/internal/database"
	"events-backend/internal/database/methods"
	"events-backend/internal/generated/restapi/operations"

	"github.com/go-openapi/runtime/middleware"
)

func GetEventHandler(params operations.GetEventParams) middleware.Responder {
	log.Printf("Hit GET /event/%d from %s\n", params.ID, params.HTTPRequest.UserAgent())

	db := database.NewDBConnector(true)
	defer db.Close()

	event, err := methods.GetEventById(db, params)
	if err != nil {
		log.Println(err)

		log.Printf("Error returning events")

		return operations.NewGetEventsBadRequest().WithPayload(&operations.GetEventsBadRequestBody{
			ErrorCode: 12,
			Status:    false,
		})
	}

	return operations.NewGetEventOK().WithPayload(
		event,
	)
}
