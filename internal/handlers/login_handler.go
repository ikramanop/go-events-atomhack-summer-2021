package handlers

import (
	"log"

	"events-backend/internal/database"
	"events-backend/internal/database/methods"
	"events-backend/internal/generated/restapi/operations"

	"github.com/go-openapi/runtime/middleware"
)

func LoginHandler(params operations.LoginParams) middleware.Responder {
	log.Printf("Hit GET /events from %s\n", params.HTTPRequest.UserAgent())

	db := database.NewDBConnector(true)
	defer db.Close()

	uuid, err := methods.ValidateUser(db, params)
	if err != nil {
		log.Println(err)

		log.Printf("Error returning uuid")

		return operations.NewLoginBadRequest().WithPayload(&operations.LoginBadRequestBody{
			ErrorCode: 12,
			Status:    false,
		})
	}

	return operations.NewLoginOK().WithPayload(
		&operations.LoginOKBody{UUID: uuid},
	).WithAccessControlAllowOrigin("*")
}
