package handlers

import (
	"log"

	"events-backend/internal/database"
	"events-backend/internal/database/methods"
	"events-backend/internal/generated/restapi/operations"

	"github.com/go-openapi/runtime/middleware"
)

func GetEventsByOrganization(params operations.GetEventsByOrganizationParams) middleware.Responder {
	log.Printf("Hit GET /events from %s\n", params.HTTPRequest.UserAgent())

	db := database.NewDBConnector(true)
	defer db.Close()

	event, err := methods.GetEventsByOrganization(db, params)
	if err != nil {
		log.Println(err)

		log.Printf("Error returning events")

		return operations.NewGetEventsByOrganizationBadRequest().WithPayload(&operations.GetEventsByOrganizationBadRequestBody{
			ErrorCode: 12,
			Status:    false,
		})
	}

	return operations.NewGetEventsByOrganizationOK().WithPayload(
		event,
	)
}
