package main

import (
	"events-backend/internal/handlers"
	"log"

	"github.com/go-openapi/loads"

	"events-backend/internal/generated/restapi"
	"events-backend/internal/generated/restapi/operations"
)

func main() {
	swaggerSpec, err := loads.Analyzed(restapi.SwaggerJSON, "")
	if err != nil {
		log.Fatalln(err)
	}

	api := operations.NewAPIAPI(swaggerSpec)
	server := restapi.NewServer(api)
	defer func() {
		if err := server.Shutdown(); err != nil {
			log.Fatalln(err)
		}
	}()

	server.Port = 8083

	api.CheckHealthHandler = operations.CheckHealthHandlerFunc(handlers.HealthCheckHandler)
	api.GetEventsHandler = operations.GetEventsHandlerFunc(handlers.GetEventsHandler)
	api.GetEventHandler = operations.GetEventHandlerFunc(handlers.GetEventHandler)
	api.GetEventsByOrganizationHandler = operations.GetEventsByOrganizationHandlerFunc(handlers.GetEventsByOrganization)
	api.GetCategoriesHandler = operations.GetCategoriesHandlerFunc(handlers.GetCategoriesHandler)
	api.GetSubCategoriesHandler = operations.GetSubCategoriesHandlerFunc(handlers.GetSubCategoriesHandler)
	api.GetSubSubCategoriesHandler = operations.GetSubSubCategoriesHandlerFunc(handlers.GetSubSubCategoriesHandler)
	api.LoginHandler = operations.LoginHandlerFunc(handlers.LoginHandler)
	api.LikeEventHandler = operations.LikeEventHandlerFunc(handlers.LikeEventHandler)
	api.DislikeEventHandler = operations.DislikeEventHandlerFunc(handlers.DislikeEventHandler)
	api.BookEventHandler = operations.BookEventHandlerFunc(handlers.BookEventHandler)
	api.GetEventsMetricsHandler = operations.GetEventsMetricsHandlerFunc(handlers.GetEventsMetricsHandler)
	api.GetEventMetricsHandler = operations.GetEventMetricsHandlerFunc(handlers.GetEventMetricsHandler)

	//database.NewDBConnector(false)

	if err := server.Serve(); err != nil {
		log.Fatalln(err)
	}
}
