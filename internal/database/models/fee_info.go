package models

type FeeInfo struct {
	tableName     struct{} `pg:"fee_info"`
	ID            int64    `pg:",pk"`
	EventID       int64
	Fee           *string
	Capacity      *int64 `pg:",use_zero"`
	BookingsCount int64  `pg:",use_zero"`
}
