package models

import "time"

type Metric struct {
	tableName  struct{} `pg:"metric"`
	ID         int64    `pg:",pk"`
	EventID    int64    `pg:",use_zero"`
	MetricName string
	Count      int64     `pg:",use_zero"`
	IssueDate  time.Time `pg:"default:now()"`
}
