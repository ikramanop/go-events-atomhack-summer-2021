package models

type Category3 struct {
	tableName   struct{} `pg:"category_3"`
	ID          int64    `pg:",pk"`
	Category2ID int64
	Name        string
	Event       *Event `pg:"rel:has-one"`
}
