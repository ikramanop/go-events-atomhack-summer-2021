package models

type EventRatings struct {
	tableName struct{} `pg:"event_ratings"`
	ID        int64    `pg:",pk"`
	EventID   int64    `pg:",use_zero"`
	Rate      bool
	Username  string
}
