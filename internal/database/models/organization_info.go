package models

type OrganizationInfo struct {
	tableName        struct{} `pg:"organization_info"`
	ID               int64    `pg:",pk"`
	OrganizationUUID string   `pg:",unique"`
	OrganizationName string   `pg:",unique"`
}
