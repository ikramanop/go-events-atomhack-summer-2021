package models

type Category1 struct {
	tableName struct{}     `pg:"category_1"`
	ID        int64        `pg:",pk"`
	Name      string       `pg:",unique"`
	Category2 *[]Category2 `pg:"rel:has-many"`
	Event     Event        `pg:"rel:has-one"`
}
