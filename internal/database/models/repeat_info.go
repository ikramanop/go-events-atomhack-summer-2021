package models

import "time"

type RepeatInfo struct {
	tableName struct{} `pg:"repeat_info"`
	ID        int64    `pg:",pk"`
	EventID   int64
	DateStart time.Time
	DateEnd   time.Time
	IsDaily   bool `pg:",use_zero"`
	Params    *struct{}
}
