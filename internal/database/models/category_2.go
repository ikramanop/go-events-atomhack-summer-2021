package models

type Category2 struct {
	tableName   struct{} `pg:"category_2"`
	ID          int64    `pg:",pk"`
	Category1ID int64
	Name        string
	Category2   *[]Category3 `pg:"rel:has-many"`
	Event       *Event       `pg:"rel:has-one"`
}
