package models

import "time"

type Event struct {
	tableName        struct{} `pg:"event"`
	ID               int64    `pg:",pk"`
	OrganizationUUID string
	Title            string
	Description      string
	Category1ID      int64  `pg:",use_zero"`
	Category2ID      *int64 `pg:",use_zero"`
	Category3ID      *int64 `pg:",use_zero"`
	Place            string
	DateStart        *time.Time
	DateEnd          *time.Time
	Params           *struct{}
	CreatedAt        time.Time       `pg:"default:now()"`
	EventRatings     []*EventRatings `pg:"rel:has-many"`
	RepeatInfo       *RepeatInfo     `pg:"rel:has-one"`
	FeeInfo          *FeeInfo        `pg:"rel:has-one"`
}
