package methods

import (
	"log"

	"github.com/go-pg/pg/v10"

	"events-backend/internal/database/models"
	dto "events-backend/internal/generated/models"
)

func GetCategories(db *pg.DB) (categories []*dto.Category, err error) {
	var categoriesDb []*models.Category1

	err = db.Model(&categoriesDb).
		Select()

	if err != nil {
		if err.Error() == "pg: no rows in result set" {
			return nil, nil
		} else {
			log.Printf("FAIL. Fatal error with get categories info: %s", err)

			return nil, err
		}
	}

	for _, category := range categoriesDb {
		categories = append(categories, &dto.Category{
			ID:   category.ID,
			Name: category.Name,
		})
	}

	return categories, err
}
