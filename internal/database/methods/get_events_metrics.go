package methods

import (
	"log"

	"github.com/go-pg/pg/v10"

	"events-backend/internal/database/models"
	dto "events-backend/internal/generated/models"
)

func GetEventsMetrics(db *pg.DB) (metricBundle []*dto.MetricBundle, err error) {
	var eventsDb []*models.Event

	err = db.Model(&eventsDb).
		Relation("EventRatings").
		Relation("FeeInfo").
		Relation("RepeatInfo").
		Order("created_at DESC").
		Select()

	if err != nil {
		if err.Error() == "pg: no rows in result set" {
			return nil, nil
		} else {
			log.Printf("FAIL. Fatal error with get events info: %s", err)

			return nil, err
		}
	}

	for _, event := range eventsDb {
		ratings := dto.Ratings{
			Likes:    0,
			Dislikes: 0,
		}
		if event.EventRatings != nil {
			for _, rating := range event.EventRatings {
				if rating.Rate {
					ratings.Likes++
				} else {
					ratings.Dislikes++
				}
			}
		}

		var dateStart *string
		if event.DateStart != nil {
			raw := event.DateStart.String()
			dateStart = &raw
		}

		var dateEnd *string
		if event.DateStart != nil {
			raw := event.DateEnd.String()
			dateEnd = &raw
		}

		var repeatInfo *dto.RepeatInfo
		if event.RepeatInfo != nil {
			repeatInfo = &dto.RepeatInfo{
				IsDaily:   event.RepeatInfo.IsDaily,
				Params:    event.RepeatInfo.Params,
				DateStart: event.RepeatInfo.DateStart.Format("Mon 15:04:05"),
				DateEnd:   event.RepeatInfo.DateEnd.Format("Mon 15:04:05"),
			}

			if repeatInfo.IsDaily {
				repeatInfo.DateStart = event.RepeatInfo.DateStart.Format("15:04:05")
				repeatInfo.DateEnd = event.RepeatInfo.DateEnd.Format("15:04:05")
			}
		}

		var metricsDb []*models.Metric

		err := db.Model(&metricsDb).
			Where("event_id = ?", event.ID).
			Select()

		if err != nil {
			if err.Error() != "pg: no rows in result set" {
				log.Printf("FAIL. Fatal error with get metrics info: %s", err)

				return nil, err
			}
		}

		var metrics []*dto.Metric
		for _, m := range metricsDb {
			metrics = append(metrics, &dto.Metric{
				Count:      m.Count,
				IssueDate:  m.IssueDate.Format("2006-01-02"),
				MetricName: m.MetricName,
			})
		}

		metricBundle = append(metricBundle, &dto.MetricBundle{
			Category1: event.Category1ID,
			Category2: event.Category2ID,
			Category3: event.Category3ID,
			DateEnd:   dateEnd,
			DateStart: dateStart,
			FeeInfo: &dto.FeeInfo{
				BookingsCount: event.FeeInfo.BookingsCount,
				Capacity:      event.FeeInfo.Capacity,
				Fee:           event.FeeInfo.Fee,
			},
			ID:         event.ID,
			Metrics:    metrics,
			Place:      event.Place,
			Ratings:    &ratings,
			RepeatInfo: repeatInfo,
		})
	}

	return metricBundle, nil
}
