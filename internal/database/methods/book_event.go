package methods

import (
	"log"

	"github.com/go-pg/pg/v10"

	"events-backend/internal/database/models"
	"events-backend/internal/generated/restapi/operations"
)

func BookEvent(db *pg.DB, params operations.BookEventParams) error {
	feeDb := new(models.FeeInfo)

	err := db.Model(feeDb).
		Where("event_id = ?", params.ID).
		Select()

	if err != nil {
		log.Printf("FAIL. Fatal error with get rate data: %s", err)

		return err
	}

	feeDb.BookingsCount++

	_, err = db.Model(feeDb).
		Where("id = ?", feeDb.ID).
		Update()
	if err != nil {
		return err
	}

	err = IncrementMetric(db, "books_metric", params.ID)
	if err != nil {
		return err
	}

	return nil
}
