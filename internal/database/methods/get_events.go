package methods

import (
	"github.com/go-pg/pg/v10"
	"log"
	"math"
	"sort"

	"events-backend/internal/database/models"
	dto "events-backend/internal/generated/models"
	"events-backend/internal/generated/restapi/operations"
)

type Liked []*dto.Event

func (e Liked) Len() int {
	return len(e)
}

func (e Liked) Swap(i, j int) {
	e[i], e[j] = e[j], e[i]
}

func (e Liked) Less(i, j int) bool {
	return e[i].Ratings.Likes < e[j].Ratings.Likes
}

type Disliked []*dto.Event

func (e Disliked) Len() int {
	return len(e)
}

func (e Disliked) Swap(i, j int) {
	e[i], e[j] = e[j], e[i]
}

func (e Disliked) Less(i, j int) bool {
	return e[i].Ratings.Dislikes < e[j].Ratings.Dislikes
}

func GetEvents(db *pg.DB, params operations.GetEventsParams) (events []*dto.Event, err error) {
	var eventsDb []*models.Event

	query := db.Model(&eventsDb).
		Relation("EventRatings").
		Relation("FeeInfo").
		Relation("RepeatInfo").
		Order("created_at DESC").
		Where("event.date_start >= now() or event.date_start is null")

	if params.Category != nil {
		category := new(models.Category1)

		err = db.Model(&category).
			Where("name = ?", params.Category).
			Select()

		if err != nil {
			if err.Error() != "pg: no rows in result set" {
				log.Printf("FAIL. Fatal error with get categories: %s", err)

				return nil, err
			}
		} else {
			query = query.Where("category1id = ?", category.ID)
		}
	}

	err = query.Select()

	if err != nil {
		if err.Error() == "pg: no rows in result set" {
			return nil, nil
		} else {
			log.Printf("FAIL. Fatal error with get events info: %s", err)

			return nil, err
		}
	}

	for _, event := range eventsDb {
		ratings := dto.Ratings{
			Likes:    0,
			Dislikes: 0,
		}
		if event.EventRatings != nil {
			for _, rating := range event.EventRatings {
				if rating.Rate {
					ratings.Likes++
				} else {
					ratings.Dislikes++
				}
			}
		}

		var dateStart *string
		if event.DateStart != nil {
			raw := event.DateStart.String()
			dateStart = &raw
		}

		var dateEnd *string
		if event.DateStart != nil {
			raw := event.DateEnd.String()
			dateEnd = &raw
		}

		var repeatInfo *dto.RepeatInfo
		if event.RepeatInfo != nil {
			repeatInfo = &dto.RepeatInfo{
				IsDaily:   event.RepeatInfo.IsDaily,
				Params:    event.RepeatInfo.Params,
				DateStart: event.RepeatInfo.DateStart.Format("Mon 15:04:05"),
				DateEnd:   event.RepeatInfo.DateEnd.Format("Mon 15:04:05"),
			}

			if repeatInfo.IsDaily {
				repeatInfo.DateStart = event.RepeatInfo.DateStart.Format("15:04:05")
				repeatInfo.DateEnd = event.RepeatInfo.DateEnd.Format("15:04:05")
			}
		}

		events = append(events, &dto.Event{
			DateEnd:   dateEnd,
			DateStart: dateStart,
			FeeInfo: &dto.FeeInfo{
				BookingsCount: event.FeeInfo.BookingsCount,
				Capacity:      event.FeeInfo.Capacity,
				Fee:           event.FeeInfo.Fee,
			},
			ID:          event.ID,
			Params:      event.Params,
			Place:       event.Place,
			RepeatInfo:  repeatInfo,
			Title:       event.Title,
			Description: event.Description,
			Ratings:     &ratings,
			Category1:   event.Category1ID,
			Category2:   event.Category2ID,
			Category3:   event.Category3ID,
		})

		err = IncrementMetric(db, "show_metric", event.ID)
		if err != nil {
			return nil, err
		}
	}

	if params.Sort != nil {
		if *params.Sort == "likes" {
			sort.Sort(Liked(events))
		}
		if *params.Sort == "dislikes" {
			sort.Sort(Disliked(events))
		}
	}

	return events[:int64(math.Min(float64(len(events)), float64(params.Limit)))], nil
}
