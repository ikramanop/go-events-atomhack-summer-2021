package methods

import (
	"events-backend/internal/database/models"
	"events-backend/internal/generated/restapi/operations"
	"github.com/go-pg/pg/v10"
	uuidd "github.com/satori/go.uuid"
	"log"
)

func ValidateUser(db *pg.DB, params operations.LoginParams) (string, error) {
	organizationInfo := new(models.OrganizationInfo)

	err := db.Model(organizationInfo).
		Where("organization_name = ?", params.OrganizationData.OrganizationName).
		Select()

	if err != nil {
		if err.Error() == "pg: no rows in result set" {
			uuid := uuidd.NewV4().String()

			organizationInfo = &models.OrganizationInfo{
				OrganizationUUID: uuid,
				OrganizationName: params.OrganizationData.OrganizationName,
			}

			_, err := db.Model(organizationInfo).Insert()
			if err != nil {
				return "", err
			}

			return uuid, nil
		} else {
			log.Printf("FAIL. Fatal error with get events info: %s", err)

			return "", err
		}
	}

	return organizationInfo.OrganizationUUID, nil
}
