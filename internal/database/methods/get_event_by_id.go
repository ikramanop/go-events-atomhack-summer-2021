package methods

import (
	"log"
	"time"

	"github.com/go-pg/pg/v10"

	"events-backend/internal/database/models"
	dto "events-backend/internal/generated/models"
	"events-backend/internal/generated/restapi/operations"
)

func GetEventById(db *pg.DB, params operations.GetEventParams) (event *dto.Event, err error) {
	eventDb := new(models.Event)

	err = db.Model(eventDb).
		Relation("EventRatings").
		Relation("RepeatInfo").
		Relation("FeeInfo").
		Where("event.id = ?", params.ID).
		Select()

	if err != nil {
		if err.Error() == "pg: no rows in result set" {
			return nil, nil
		} else {
			log.Printf("FAIL. Fatal error with get event: %s", err)

			return nil, err
		}
	}

	ratings := dto.Ratings{
		Likes:    0,
		Dislikes: 0,
	}
	if eventDb.EventRatings != nil {
		for _, rating := range eventDb.EventRatings {
			if rating.Rate {
				ratings.Likes++
			} else {
				ratings.Dislikes++
			}
		}
	}

	var dateStart *string
	if eventDb.DateStart != nil {
		raw := eventDb.DateStart.String()
		dateStart = &raw
	}

	var dateEnd *string
	if eventDb.DateStart != nil {
		raw := eventDb.DateEnd.String()
		dateEnd = &raw
	}

	var repeatInfo *dto.RepeatInfo
	if eventDb.RepeatInfo != nil {
		repeatInfo = &dto.RepeatInfo{
			IsDaily:   eventDb.RepeatInfo.IsDaily,
			Params:    eventDb.RepeatInfo.Params,
			DateStart: eventDb.RepeatInfo.DateStart.Format("Mon 15:04:05"),
			DateEnd:   eventDb.RepeatInfo.DateEnd.Format("Mon 15:04:05"),
		}

		if repeatInfo.IsDaily {
			repeatInfo.DateStart = eventDb.RepeatInfo.DateStart.Format(time.Kitchen)
			repeatInfo.DateEnd = eventDb.RepeatInfo.DateEnd.Format(time.Kitchen)
		}
	}

	event = &dto.Event{
		DateEnd:   dateEnd,
		DateStart: dateStart,
		FeeInfo: &dto.FeeInfo{
			BookingsCount: eventDb.FeeInfo.BookingsCount,
			Capacity:      eventDb.FeeInfo.Capacity,
			Fee:           eventDb.FeeInfo.Fee,
		},
		ID:          eventDb.ID,
		Params:      eventDb.Params,
		Place:       eventDb.Place,
		RepeatInfo:  repeatInfo,
		Title:       eventDb.Title,
		Description: eventDb.Description,
		Ratings:     &ratings,
		Category1:   eventDb.Category1ID,
		Category2:   eventDb.Category2ID,
		Category3:   eventDb.Category3ID,
	}

	err = IncrementMetric(db, "click_metric", event.ID)
	if err != nil {
		return nil, err
	}

	return event, nil
}
