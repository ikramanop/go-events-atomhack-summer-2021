package methods

import (
	"log"

	"github.com/go-pg/pg/v10"

	"events-backend/internal/database/models"
	"events-backend/internal/generated/restapi/operations"
)

func DislikeEvent(db *pg.DB, params operations.DislikeEventParams) (bool, error) {
	dislikeDb := new(models.EventRatings)

	err := db.Model(dislikeDb).
		Where("event_id = ? and username = ?", params.ID, params.RateData.Username).
		Select()

	if err != nil {
		if err.Error() == "pg: no rows in result set" {
			dislikeDb = &models.EventRatings{
				EventID:  params.ID,
				Rate:     false,
				Username: params.RateData.Username,
			}

			_, err := db.Model(dislikeDb).Insert()
			if err != nil {
				return false, err
			}

			return true, nil
		} else {
			log.Printf("FAIL. Fatal error with get rate data: %s", err)

			return false, err
		}
	}

	return false, nil
}
