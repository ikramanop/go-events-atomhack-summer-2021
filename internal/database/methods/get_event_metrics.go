package methods

import (
	"log"

	"github.com/go-pg/pg/v10"

	"events-backend/internal/database/models"
	dto "events-backend/internal/generated/models"
	"events-backend/internal/generated/restapi/operations"
)

func GetEventMetrics(db *pg.DB, params operations.GetEventMetricsParams) (metricBundle *dto.MetricBundle, err error) {
	eventDb := new(models.Event)

	err = db.Model(eventDb).
		Relation("EventRatings").
		Relation("RepeatInfo").
		Relation("FeeInfo").
		Where("event.id = ?", params.ID).
		Select()

	if err != nil {
		if err.Error() == "pg: no rows in result set" {
			return nil, nil
		} else {
			log.Printf("FAIL. Fatal error with get events info: %s", err)

			return nil, err
		}
	}

	ratings := dto.Ratings{
		Likes:    0,
		Dislikes: 0,
	}
	if eventDb.EventRatings != nil {
		for _, rating := range eventDb.EventRatings {
			if rating.Rate {
				ratings.Likes++
			} else {
				ratings.Dislikes++
			}
		}
	}

	var dateStart *string
	if eventDb.DateStart != nil {
		raw := eventDb.DateStart.String()
		dateStart = &raw
	}

	var dateEnd *string
	if eventDb.DateStart != nil {
		raw := eventDb.DateEnd.String()
		dateEnd = &raw
	}

	var repeatInfo *dto.RepeatInfo
	if eventDb.RepeatInfo != nil {
		repeatInfo = &dto.RepeatInfo{
			IsDaily:   eventDb.RepeatInfo.IsDaily,
			Params:    eventDb.RepeatInfo.Params,
			DateStart: eventDb.RepeatInfo.DateStart.Format("Mon 15:04:05"),
			DateEnd:   eventDb.RepeatInfo.DateEnd.Format("Mon 15:04:05"),
		}

		if repeatInfo.IsDaily {
			repeatInfo.DateStart = eventDb.RepeatInfo.DateStart.Format("15:04:05")
			repeatInfo.DateEnd = eventDb.RepeatInfo.DateEnd.Format("15:04:05")
		}
	}

	var metricsDb []*models.Metric

	err = db.Model(&metricsDb).
		Where("event_id = ?", eventDb.ID).
		Select()

	if err != nil {
		if err.Error() != "pg: no rows in result set" {
			log.Printf("FAIL. Fatal error with get metrics info: %s", err)

			return nil, err
		}
	}

	var metrics []*dto.Metric
	for _, m := range metricsDb {
		metrics = append(metrics, &dto.Metric{
			Count:      m.Count,
			IssueDate:  m.IssueDate.Format("2006-01-02"),
			MetricName: m.MetricName,
		})
	}

	metricBundle = &dto.MetricBundle{
		Category1: eventDb.Category1ID,
		Category2: eventDb.Category2ID,
		Category3: eventDb.Category3ID,
		DateEnd:   dateEnd,
		DateStart: dateStart,
		FeeInfo: &dto.FeeInfo{
			BookingsCount: eventDb.FeeInfo.BookingsCount,
			Capacity:      eventDb.FeeInfo.Capacity,
			Fee:           eventDb.FeeInfo.Fee,
		},
		ID:         eventDb.ID,
		Metrics:    metrics,
		Place:      eventDb.Place,
		Ratings:    &ratings,
		RepeatInfo: repeatInfo,
	}

	return metricBundle, nil
}
