package methods

import (
	"log"
	"time"

	"github.com/go-pg/pg/v10"

	"events-backend/internal/database/models"
	dto "events-backend/internal/generated/models"
	"events-backend/internal/generated/restapi/operations"
)

func GetEventsByOrganization(db *pg.DB, params operations.GetEventsByOrganizationParams) (events []*dto.Event, err error) {
	var eventsDb []*models.Event

	err = db.Model(&eventsDb).
		Relation("EventRatings").
		Relation("RepeatInfo").
		Relation("FeeInfo").
		Where("organization_uuid = ?", params.Organization).
		Select()

	if err != nil {
		if err.Error() == "pg: no rows in result set" {
			return nil, nil
		} else {
			log.Printf("FAIL. Fatal error with get posts info: %s", err)

			return nil, err
		}
	}

	for _, event := range eventsDb {
		ratings := dto.Ratings{
			Likes:    0,
			Dislikes: 0,
		}
		if event.EventRatings != nil {
			for _, rating := range event.EventRatings {
				if rating.Rate {
					ratings.Likes++
				} else {
					ratings.Dislikes++
				}
			}
		}

		var dateStart *string
		if event.DateStart != nil {
			raw := event.DateStart.String()
			dateStart = &raw
		}

		var dateEnd *string
		if event.DateStart != nil {
			raw := event.DateEnd.String()
			dateEnd = &raw
		}

		var repeatInfo *dto.RepeatInfo
		if event.RepeatInfo != nil {
			repeatInfo = &dto.RepeatInfo{
				IsDaily:   event.RepeatInfo.IsDaily,
				Params:    event.RepeatInfo.Params,
				DateStart: event.RepeatInfo.DateStart.Format("Mon 15:04:05"),
				DateEnd:   event.RepeatInfo.DateEnd.Format("Mon 15:04:05"),
			}

			if repeatInfo.IsDaily {
				repeatInfo.DateStart = event.RepeatInfo.DateStart.Format(time.Kitchen)
				repeatInfo.DateEnd = event.RepeatInfo.DateEnd.Format(time.Kitchen)
			}
		}

		events = append(events, &dto.Event{
			DateEnd:   dateEnd,
			DateStart: dateStart,
			FeeInfo: &dto.FeeInfo{
				BookingsCount: event.FeeInfo.BookingsCount,
				Capacity:      event.FeeInfo.Capacity,
				Fee:           event.FeeInfo.Fee,
			},
			ID:          event.ID,
			Params:      event.Params,
			Place:       event.Place,
			RepeatInfo:  repeatInfo,
			Title:       event.Title,
			Description: event.Description,
			Ratings:     &ratings,
			Category1:   event.Category1ID,
			Category2:   event.Category2ID,
			Category3:   event.Category3ID,
		})
	}

	return events, nil
}
