package methods

import (
	"events-backend/internal/database/models"
	"github.com/go-pg/pg/v10"
	"log"
)

func IncrementMetric(db *pg.DB, metricName string, eventID int64) error {
	metric := new(models.Metric)

	err := db.Model(metric).
		Where("event_id = ?", eventID).
		Where("metric_name = ?", metricName).
		Where("date(issue_date) = date(now())").
		Select()

	if err != nil {
		if err.Error() == "pg: no rows in result set" {
			metric = &models.Metric{
				EventID:    eventID,
				MetricName: metricName,
				Count:      1,
			}

			_, err := db.Model(metric).Insert()
			if err != nil {
				return err
			}

			return nil
		} else {
			log.Printf("FAIL. Fatal error with metric increment: %s", err)

			return err
		}
	}

	metric.Count++

	_, err = db.Model(metric).Where("id = ?", metric.ID).Update()
	if err != nil {
		return err
	}

	return nil
}
