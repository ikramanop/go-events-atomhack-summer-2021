package database

import (
	"events-backend/internal/database/models"
	"fmt"

	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
)

func NewDBConnector(isDBCreated bool) *pg.DB {
	db := pg.Connect(&pg.Options{
		Addr:     "146.59.45.210:5432",
		User:     "root",
		Password: "root",
		Database: "atom_hack",
	})

	if !isDBCreated {
		err := createSchema(db)
		if err != nil {
			panic(fmt.Errorf("FAIL. Fatal error with db connector: %s", err))
		}
	}

	return db
}

func createSchema(db *pg.DB) error {
	modelsInit := []interface{}{
		(*models.Event)(nil),
		(*models.EventRatings)(nil),
		(*models.RepeatInfo)(nil),
		(*models.FeeInfo)(nil),
		(*models.Category1)(nil),
		(*models.Category2)(nil),
		(*models.Category3)(nil),
		(*models.OrganizationInfo)(nil),
		(*models.Metric)(nil),
	}

	for _, model := range modelsInit {
		err := db.Model(model).CreateTable(&orm.CreateTableOptions{
			Temp: false,
		})
		if err != nil {
			return err
		}
	}

	return nil
}
