FROM golang:alpine as builder

ENV PATH=$PATH:$GOROOT/bin:$GOPATH/bin

RUN apk add --update --no-cache alpine-sdk bash ca-certificates \
      libressl \
      tar \
      git openssh openssl yajl-dev zlib-dev cyrus-sasl-dev openssl-dev coreutils curl py-pip

RUN pip3 install PyYAML

WORKDIR /src/app
RUN git clone https://github.com/go-swagger/go-swagger
WORKDIR /src/app/go-swagger
RUN go install ./cmd/swagger

WORKDIR /src/app
COPY . /src/app

RUN make swagger-gen
RUN make swagger-doc

RUN go mod download
RUN go mod vendor

RUN make build

FROM nginx:alpine as runner

COPY --from=builder /src/app/bin .
COPY --from=builder /src/app/doc ./doc

RUN ./events-backend &

COPY conf/default.conf /etc/nginx/conf.d/default.conf
COPY run-backend.sh .

EXPOSE 80

RUN chmod +x run-backend.sh

ENTRYPOINT ["sh", "run-backend.sh"]
